﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace taller3
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}
        async private void pasarpersonas(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new crearpersonas());
        }
        async private void pasarusuarios(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new crearusuarios());
        }

    }
}
