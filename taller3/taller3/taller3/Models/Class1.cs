﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace taller3.Models
{
    class Class1
    {

        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string Name { get; set; }

        public int telefono { get; set; }

        public string email { get; set; }

        public char sexo { get; set; }
    }
}
