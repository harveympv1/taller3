﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace taller3.Models
{
    class Personas
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string Name { get; set; }

        public int telefono { get; set; }

        public string email { get; set; }

        public char sexo { get; set; }
    }
}
