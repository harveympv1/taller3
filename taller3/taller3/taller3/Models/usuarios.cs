﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace taller3.Models
{
    class usuarios
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string nombre_usuario { get; set; }

        public string password { get; set; }

        public string avatar { get; set; }

        public Boolean estado { get; set; }

    }
}
