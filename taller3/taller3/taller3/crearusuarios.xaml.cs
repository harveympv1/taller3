﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace taller3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class crearusuarios : ContentPage
	{
		public crearusuarios ()
		{
			InitializeComponent ();
		}
        async private void listarusuarios(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new listarusuarios());
        }
    }
}